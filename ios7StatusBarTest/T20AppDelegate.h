//
//  T20AppDelegate.h
//  ios7StatusBarTest
//
//  Created by Chris Ledden on 8/12/13.
//  Copyright (c) 2013 Twenty20Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface T20AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
