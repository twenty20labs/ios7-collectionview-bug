//
//  main.m
//  ios7StatusBarTest
//
//  Created by Chris Ledden on 8/12/13.
//  Copyright (c) 2013 Twenty20Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "T20AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([T20AppDelegate class]));
    }
}
